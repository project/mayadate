<?php
// library to transform a gregorian date to a GMT Maya Long Count

// formats for the Maya date
define('MAYADATE_FORMAT_TEXT', 0);
define('MAYADATE_FORMAT_VC_HORIZONTAL', 1);
define('MAYADATE_FORMAT_VC_VERTICAL', 2);
define('MAYADATE_FORMAT_HC_HORIZONTAL', 3);
define('MAYADATE_FORMAT_HC_VERTICAL', 4);

/** \brief The Maya date format
 *
 * \li MAYADATE_FORMAT_TEXT
 * \li MAYADATE_FORMAT_VC_HORIZONTAL
 * \li MAYADATE_FORMAT_VC_VERTICAL
 * \li MAYADATE_FORMAT_HC_HORIZONTAL
 * \li MAYADATE_FORMAT_HC_VERTICAL
 *
 * The text format gives you the Maya date in text form (ASCII).
 *
 * The Horizontal and Vertical formats generate an image with the Maya
 * date using Maya characters. In this case you can specify the width
 * of the dot.
 *
 * VC means vertical characters (the dots & lines got up and down)
 * whereas HC means horizontal (the dots & lines got left to right.)
 *
 * The VC_VERTICAL format is the proper Maya format, although it
 * looks like they would use horizontal writing interchangeably
 * it most often is vertical, top to bottom.
 */
class MayaFormat {
  var $format;      // one of the MAYADATE_FORMAT_...
  var $separator;   // character used to separate arabic numerals
  var $width;       // the width of one dot or bar
  var $background;  // color of the background in RGB (i.e. 0xFFFFFF for white, 0x000000 for black)
  var $color;       // the color of the dots and bars in RGB
  //var $bck_alpha -- tested with alpha but there are MANY problems with the ImageGD2 and alpha...
  //var $txt_alpha -- idem

  function set_bg_color($r, $g, $b) {
    $r = $r > 255 ? 255 : ($r < 0 ? $r = 0 : $r);
    $g = $g > 255 ? 255 : ($g < 0 ? $g = 0 : $g);
    $b = $b > 255 ? 255 : ($b < 0 ? $b = 0 : $b);
    $this->background = ($r << 16) | ($g << 8) | $b;
  }
  function set_color($r, $g, $b) {
    $r = $r > 255 ? 255 : ($r < 0 ? $r = 0 : $r);
    $g = $g > 255 ? 255 : ($g < 0 ? $g = 0 : $g);
    $b = $b > 255 ? 255 : ($b < 0 ? $b = 0 : $b);
    $this->color = ($r << 16) | ($g << 8) | $b;
  }
  function get_bg_color() {
    $c = array();
    $b = hexdec($this->background);
    $c[0] = ($b >> 16) & 0xFF;
    $c[1] = ($b >>  8) & 0xFF;
    $c[2] = $b & 0xFF;
    return $c;
  }
  function get_color() {
    $c = array();
    $t = hexdec($this->color);
    $c[0] = ($t >> 16) & 0xFF;
    $c[1] = ($t >>  8) & 0xFF;
    $c[2] = $t & 0xFF;
    return $c;
  }
}

/** \brief Determins the width of a number in pixels
 *
 * The characters are expected to be render as:
 *
 * width / 4 + width + width / 4 + width + width / 4 ...
 *
 * So each character is separated by $width / 2
 *
 * \param[in] $number The number to be checked
 * \param[in] $w The width of a dot in pixels
 *
 * \return The width in pixels
 */
function maya_numeral_width($number, $w) {
  // The zero is about 3 bars wide
  if ($number == 0) {
    $number = 15;
  }
  if ($number <= 5) {
    // 1/2 + 2/2 + 1/2
    return $w * 2;
  }
  if ($number <= 10) {
    // 2/4 + 4/4 + 1/4 + 4/4 + 2/4
    return $w * 13 / 4;
  }
  if ($number <= 15) {
    // 2/4 + 4/4 + 1/4 + 4/4 + 1/4 + 4/4 + 2/4
    return $w * 9 / 2;
  }
  // 2/4 + 4/4 + 1/4 + 4/4 + 1/4 + 4/4 + 1/4 + 4/4 + 2/4
  return $w * 23 / 4;
}


/** \brief Compute the next position depending on the orientation
 *
 * This function computes the position for the next dot or line
 * using the orientation parameter accordingly.
 *
 * The function modifies the x and y coordinates.
 *
 * \param[in,out] $x The horizontal position
 * \param[in,out] $y The vertical position
 * \param[in] $w The width of one dot
 * \param[in] $o The orientation (0 - vertical characters, 1 - horizontal characters)
 */
function _maya_next_position(&$x, &$y, $w, $o) {
  if ($o == 0) {
    $x += $w + $w / 4;
  }
  else {
    $y += $w + $w / 4;
  }
}


/** \brief Draw a Maya zero which is a shell.
 *
 * The Maya zero was represented by a shell. Kind of like our
 * zero on the side with strips. An interesting concept if I
 * may say.
 *
 * The function draws the zero horizontally or vertically. Zero
 * uses as much space as the number 15.
 *
 * \param[in] $img The image resource where the zero is drawn.
 * \param[in] $x The left coordinate
 * \param[in] $y The top coordinate
 * \param[in] $w The width of a pointer
 * \param[in] $c The color used to render the zero
 * \param[in] $o The orientation of the zero (0 - horizontal, 1 - vertical)
 */
function _maya_numeral_draw_zero($img, $x, $y, $w, $c, $o) {
  $t = $w / 10;
  if($t < 4) {
    $t = 4;
  }
  imagesetthickness($img, $t);

  if ($o == 1) {
    $cx = $x + $w * 5 / 2 - 4; // 4 is the thickness
    $cy = $y + $w * 5 / 4;

    imagefilledarc($img, $cx,            $cy, $w * 5, $w * 2, 0, 360, $c, IMG_ARC_PIE | IMG_ARC_NOFILL);
    imagefilledarc($img, $cx,            $cy - $w * 0.25, $w * 4.8, $w * 1.5, 0, 360, $c, IMG_ARC_PIE | IMG_ARC_NOFILL);
    imagefilledarc($img, $cx - $w * 0.5, $cy - $w * 0.25, $w * 2, $w * 1.5, 120, 240, $c, IMG_ARC_PIE | IMG_ARC_NOFILL);
    imagefilledarc($img, $cx + $w * 0.5, $cy - $w * 0.25, $w * 2, $w * 1.5, 100, 260, $c, IMG_ARC_PIE | IMG_ARC_NOFILL);
    imagefilledarc($img, $cx + $w * 1.5, $cy - $w * 0.25, $w * 2, $w * 1.5, 120, 240, $c, IMG_ARC_PIE | IMG_ARC_NOFILL);
  }
  else {
    // in this case, $x is adjusted by the width of the zero
    // which is "wrong" since we don't turn the zero
    $x_offset = maya_numeral_width(0, $w);
    $cx = $x + $x_offset / 2 - $w + 4;
    $cy = $y + $w * 19 / 8;

    imagefilledarc($img, $cx,             $cy,            $w * 2,   $w * 5,     0, 360, $c, IMG_ARC_PIE | IMG_ARC_NOFILL);
    imagefilledarc($img, $cx - $w * 0.25, $cy,            $w * 1.5, $w * 4.8,   0, 360, $c, IMG_ARC_PIE | IMG_ARC_NOFILL);
    imagefilledarc($img, $cx - $w * 0.25, $cy + $w * 0.5, $w * 1.5, $w * 2,    30, 150, $c, IMG_ARC_PIE | IMG_ARC_NOFILL);
    imagefilledarc($img, $cx - $w * 0.25, $cy - $w * 0.5, $w * 1.5, $w * 2,    10, 170, $c, IMG_ARC_PIE | IMG_ARC_NOFILL);
    imagefilledarc($img, $cx - $w * 0.25, $cy - $w * 1.5, $w * 1.5, $w * 2,    30, 150, $c, IMG_ARC_PIE | IMG_ARC_NOFILL);
  }

  imagesetthickness($img, 1);
}


/** \brief Render a set of dots
 *
 * This function expects the x/y coordinates at the top-left
 * corner of the character.
 *
 * It computes the position of the character using a centering
 * algorithm which is not automatically correct historically,
 * but kind of makes sense for our purpose.
 *
 * For horizontal characters, we expect the width to be:
 *
 * $w * 4 + ($w / 4) * 3
 *
 * i.e. Dots are being spaced by 1/4th of their width and there
 * are up to 3 spaces. So this is the total width for one character.
 *
 * To center the characters we do with:
 *
 * \code
 * $total_width = $w * 4 + ($w / 4 ) * 3;
 * $width = $w * $count + ($w / 4) * ($count - 1);
 * $offset = ($total_width - $width) / 2;
 * \endcode
 *
 * Then we apply the offset to $x (horizontal) or $y (vertical)
 *
 * \param[in] $img The image resource where we render the result
 * \param[in] $count The number of dots to be rendered, 1 or more
 * \param[in] $x The left position of this row
 * \param[in] $y The top position of this row
 * \param[in] $w The width of a dot
 * \param[in] $c The color to render the dot
 * \param[in] $o The orientation (0 - vertical characters, 1 - horizontal characters)
 */
function _maya_numeral_draw_dot($img, $count, $x, $y, $w, $c, $o) {
  // compute the start position depending on the number of dots
  $total_width = $w * 4 + ($w / 4 ) * 3;
  $width = $w * $count + ($w / 4) * ($count - 1);
  $offset = ($total_width - $width) / 2;

  // move to the center of the dot
  $cx = $x + $w / 2;
  $cy = $y + $w / 2;
  if ($o == 1) {
    $cx += $offset;
  }
  else {
    $cy += $offset;
  }

  for(; $count > 0; --$count) {
    imagefilledarc($img, $cx, $cy, $w, $w, 0, 360, $c, IMG_ARC_PIE);
    _maya_next_position($cx, $cy, $w, $o ^ 1);
  }
}


/** \brief Draw one to three lines.
 *
 * This function draws one to three lins as specified.
 *
 * See the dot function for more info about the position computations.
 *
 * \param[in] $img The image resource
 * \param[in] $count The number of lines (1, 2 or 3)
 * \param[in] $x The left position of the bar
 * \param[in] $y The top position of the bar
 * \param[in] $w The width of the character
 * \param[in] $c The color to use to render the character
 * \param[in] $o The orientation (0 - vertical characters, 1 - horizontal characters)
 */
function _maya_numeral_draw_line($img, $count, $x, $y, $w, $c, $o) {
  $total_width = $w * 4 + ($w / 4 ) * 3;

  if($o == 1) {
    $width = $total_width;
    $height = $w / 2;
  }
  else {
    $width = $w / 2;
    $height = $total_width;
  }

  for(; $count > 0; --$count) {
    imagefilledrectangle($img, $x, $y, $x + $width, $y + $height, $c);
    _maya_next_position($x, $y, $w * 2.5 / 3, $o);
  }
}



/** \brief Render one numeral.
 *
 * This function renders one Maya numeral.
 *
 * See the dot function for more info about the position computation.
 *
 * \prarm[in] $img The image resource
 * \param[in] $number The digit to render
 * \param[in] $x The horizontal position where the number is rendered
 * \param[in] $y The vertical position where the number is rendered
 * \param[in] $w The size of the character
 * \param[in] $c The color used to render the numeral
 * \param[in] $o The orientation (0 - vertical characters, 1 - horizontal characters)
 */
function maya_numeral_draw($img, $number, $x, $y, $w, $c, $o) {
  switch($number) {
  default:
    // wrong number?!
    break;

  case 0:
    _maya_numeral_draw_zero($img, $x, $y, $w, $c, $o);
    break;

  case 1:
  case 2:
  case 3:
  case 4:
    _maya_numeral_draw_dot($img, $number, $x, $y, $w, $c, $o);
    break;

  case 5:
  case 10:
  case 15:
    _maya_numeral_draw_line($img, $number / 5, $x, $y, $w, $c, $o);
    break;

  case 6:
  case 7:
  case 8:
  case 9:
    _maya_numeral_draw_dot($img, $number - 5, $x, $y, $w, $c, $o);
    _maya_next_position($x, $y, $w, $o);
    _maya_numeral_draw_line($img, 1, $x, $y, $w, $c, $o);
    break;

  case 11:
  case 12:
  case 13:
  case 14:
    _maya_numeral_draw_dot($img, $number - 10, $x, $y, $w, $c, $o);
    _maya_next_position($x, $y, $w, $o);
    _maya_numeral_draw_line($img, 2, $x, $y, $w, $c, $o);
    break;

  case 16:
  case 17:
  case 18:
  case 19:
    _maya_numeral_draw_dot($img, $number - 15, $x, $y, $w, $c, $o);
    _maya_next_position($x, $y, $w, $o);
    _maya_numeral_draw_line($img, 3, $x, $y, $w, $c, $o);
    break;

  }
}

/** \brief Transform the Unix time $t into a GMT Maya Long Count representation.
 *
 * The result is a string of 5 numbers.
 *
 * The input time can be negative.
 *
 * The function accepts a format parameter (\p $f). The format is a
 * MayaFormat object.
 *
 * \bug
 * This function assumes that you have the Image GD2 installed. The
 * function does NOT check for its availability for speed reasons.
 *
 * \param[in] $t A unix time in seconds since Jan 1, 1970
 * \param[in] $f An object describing the image format
 *
 * \return The time $t in GMT Maya Long Count representation
 */
function date_to_maya($t, $f = NULL) {
  if (!$f) {
    $f = new MayaFormat;
    $f->format = MAYADATE_FORMAT_TEXT; // other fields ignored in this case
    $f->separator = '.';
  }

  // we remove the seconds, minutes and hours
  $t = floor($t / 86400);

  // Adjust the number to match the number of days in Maya Long Count
  $t += 1856305;

  // Compute the result
  $kin = $t % 20;
  $t /= 20;
  $winal = $t % 18;
  $t /= 18;
  $tun = $t % 20;
  $t /= 20;
  $katun = $t % 20;
  $t /= 20;
  $baktun = $t % 20; // goes back to 0 on Dec 21, 2012

  if($f->format == MAYADATE_FORMAT_TEXT) {
    // return the result in modern writing
    $s = $f->separator;
    return "{$baktun}$s{$katun}$s{$tun}$s{$winal}$s{$kin}";
  }

  // Fix the width, just in case
  $w = $f->width;
  if ($w < 5) {
    $w = 5;
  }
  else if ($w > 100) {
    $w = 100;
  }
  $w *= 4;

  // get all the sizes so we can compute the size of our image
  $baktun_width = maya_numeral_width($baktun, $w);
  $katun_width  = maya_numeral_width($katun,  $w);
  $tun_width    = maya_numeral_width($tun,    $w);
  $winal_width  = maya_numeral_width($winal,  $w);
  $kin_width    = maya_numeral_width($kin,    $w);

  $max_width = max($baktun_width, $katun_width, $tun_width, $winal_width, $kin_width);

  // the "height" (which may be used as the width) represents one character with 1/4th
  // the width of a dot added before, between and after each dot.
  // i.e. width * 4 + width * 3 / 4 + width * 2 / 4 = width * 21 / 4
  // (4 x 4/4th + 5 x 1/4th) -- 4 W 4 W 4 W 4 W 4
  $character_height = ($w * 21) / 4;

  $x = $w / 2;
  $y = $w / 2;

  switch($f->format) {
  case MAYADATE_FORMAT_VC_HORIZONTAL:
    // 5 characters area rendered vertical, use their respective width
    $width = $baktun_width + $katun_width + $tun_width + $winal_width + $kin_width;
    // 4 dots + 5 spaces of 1/4th the width
    $height = $character_height + $w / 2;
    $o = 0;
    $x_offsets = array(0, $baktun_width, $baktun_width + $katun_width,
                       $baktun_width + $katun_width + $tun_width,
                       $baktun_width + $katun_width + $tun_width + $winal_width);
    $y_offsets = array(0, 0, 0, 0, 0);
    break;

  default: //case MAYADATE_FORMAT_VC_VERTICAL:
    $width = $max_width - $w;
    $height = $character_height * 5 + $w / 2;
    $o = 0;
    $x_offsets = array(($max_width - $baktun_width) / 2, ($max_width - $katun_width) / 2,
                       ($max_width - $tun_width) / 2, ($max_width - $winal_width) / 2,
                       ($max_width - $kin_width) / 2);
    $y_offsets = array(0, $character_height, $character_height * 2,
                       $character_height * 3, $character_height * 4);
    break;

  case MAYADATE_FORMAT_HC_HORIZONTAL:
    // 5 characters are renreder horizontally, the width of one is the $character_height
    $width = $character_height * 5 + $w / 2;
    $height = $max_width - $w / 2;
    $o = 1;
    $x_offsets = array(0, $character_height, $character_height * 2,
                       $character_height * 3, $character_height * 4);
    $y_offsets = array($max_width - $baktun_width, $max_width - $katun_width,
                       $max_width - $tun_width, $max_width - $winal_width,
                       $max_width - $kin_width);
    break;

  case MAYADATE_FORMAT_HC_VERTICAL:
    $width = $character_height + $w / 2;
    $height = $baktun_width + $katun_width + $tun_width + $winal_width + $kin_width - $w / 2;
    $o = 1;
    $x_offsets = array(0, 0, 0, 0, 0);
    $y_offsets = array(0, $baktun_width, $baktun_width + $katun_width,
                       $baktun_width + $katun_width + $tun_width,
                       $baktun_width + $katun_width + $tun_width + $winal_width);
    break;

  }

  $img = imagecreate($width, $height);
  $c = $f->get_bg_color();
  /* $background_color = */ imagecolorallocate($img, $c[0], $c[1], $c[2]);

  $c = $f->get_color();
  $text_color = imagecolorallocate($img, $c[0], $c[1], $c[2]);

  maya_numeral_draw($img, $baktun, $x + $x_offsets[0], $y + $y_offsets[0], $w, $text_color, $o);
  maya_numeral_draw($img, $katun,  $x + $x_offsets[1], $y + $y_offsets[1], $w, $text_color, $o);
  maya_numeral_draw($img, $tun,    $x + $x_offsets[2], $y + $y_offsets[2], $w, $text_color, $o);
  maya_numeral_draw($img, $winal,  $x + $x_offsets[3], $y + $y_offsets[3], $w, $text_color, $o);
  maya_numeral_draw($img, $kin,    $x + $x_offsets[4], $y + $y_offsets[4], $w, $text_color, $o);

  $result = imagecreatetruecolor($width / 4, $height / 4);
  imagecopyresampled($result, $img, 0, 0, 0, 0, $width / 4, $height / 4, $width, $height);

  return $result;
}
