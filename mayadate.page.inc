<?php
// $ Id: $

/**
 * @author Alexis Wilke
 * @file
 * The Maya Date callback function to render the image using
 * Maya numerals.
 */
require_once 'mayadate.inc';

/** \brief Callback to generate the Maya image.
 *
 * This function calls the date_to_maya() function to generate
 * an image with Maya numerals representing a specified date.
 *
 * \param[in] $f The format (as in the input format)
 * \param[in] $d The date to represent in Maya date
 */
function mayadate_image($f, $d) {
  // defines the image parameters from the fiter
  $format = new MayaFormat;
  $format->format = variable_get('mayadate_filter_date_'.$f, 0);
  $format->width = variable_get('mayadate_filter_width_'.$f, 10);
  $format->background = variable_get('mayadate_filter_background_color_'.$f, '#ffffff');
  $format->color = variable_get('mayadate_filter_text_color_'.$f, '#000000');

  // transform from a string to an hex number
  $format->background = str_replace('#', '0x', $format->background);
  $format->color = str_replace('#', '0x', $format->color);

  $t = strtotime($d);
  $img = date_to_maya($t, $format);

  // make sure the format was valid
  if(is_string($img)) {
    drupal_set_message(t("Invalid format used? Input format is: !input_format (Date format: {$format->format}.) The date_to_maya() function did not return an image as expected.\n", array('!input_format' => $f)), 'error');
    drupal_not_found();
    return;
  }

  // output image
  drupal_set_header('Content-type: image/png');
  imagepng($img);
  imagedestroy($img);

  // allow modules to run their hook_exit() function
  drupal_page_footer();
  return;
}

